﻿namespace Solid.Models.Notification
{
    public interface IMessenger
    {
        void Send();
    }
}