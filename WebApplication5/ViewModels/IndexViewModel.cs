﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication5.Models;

namespace WebApplication5.ViewModels
{
    /// <summary>
    /// View Model for page Index.cshtml of Teacher controller
    /// and all partial views it contains
    /// </summary>
    public class IndexViewModel
    {
        // Index.cshtml
        public IEnumerable<TeacherViewModel> Teachers { get; set; }



        // _CreatePartial.cshtml
        public CreateTeacherViewModel CreateTeacherViewModel { get; set; }
    }
}